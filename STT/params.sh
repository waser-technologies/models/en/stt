#!/bin/sh

set -xe

# This file will contain default values

export IMPORTERS_VALIDATE_LOCALE=""

export M_AILABS_LANG=""
export M_AILABS_SKIP=""

export LM_ICONV_LOCALE=""

export MODEL_EXPORT_SHORT_LANG=""
export MODEL_EXPORT_LONG_LANG=""
export MODEL_EXPORT_ZIP_LANG=""