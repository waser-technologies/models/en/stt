#!/bin/bash

set -xe

pushd $STT_DIR

    if [ ! -f "/mnt/extracted/en/mls_lm.txt" ]; then

        if [ "${LM_ADD_EXCLUDED_MAX_SEC}" = "1" ]; then
            
            wget --directory-prefix /mnt/extracted/en/data/MLS --continue https://dl.fbaipublicfiles.com/mls/mls_lm_english.tar.gz 
            tar xf /mnt/extracted/en/data/MLS/mls_lm_english.tar.gz -C /mnt/extracted/en/data/MLS
            mv /mnt/extracted/en/data/MLS/mls_lm_english/data.txt /mnt/extracted/en/mls_lm.txt
        fi;
    fi;
popd