#!/bin/bash

set -xe

if [ ! -f "en/wiki_en_lower.txt" ]; then
	curl -sSL https://gitlab.com/waser-technologies/data/lm/en/wiki-dump/-/raw/main/wiki.en.txt?inline=false | tr '[:upper:]' '[:lower:]' > en/wiki_en_lower.txt
fi;

if [ "${ENGLISH_COMPATIBLE}" = "1" -a ! -f "en/_wiki_en_lower.txt" ]; then
	mv en/wiki_en_lower.txt en/_wiki_en_lower.txt
	uni2ascii -q en/_wiki_en_lower.txt > en/wiki_en_lower.txt
fi;

if [ "${LM_ADD_EXCLUDED_MAX_SEC}" = "1" ] && [ ! -f "en/excluded_max_sec_lm.txt" ]; then
	cat en/_*_lm.txt | tr '[:upper:]' '[:lower:]' > en/excluded_max_sec_lm.txt
	EXCLUDED_LM_SOURCE="en/excluded_max_sec_lm.txt"
elif [ "${LM_ADD_EXCLUDED_MAX_SEC}" = "1" ] && [ -f "en/excluded_max_sec_lm.txt" ]; then
	EXCLUDED_LM_SOURCE="en/excluded_max_sec_lm.txt"
fi;

# Remove special-char <s> that will make KenLM tools choke:
# kenlm/lm/builder/corpus_count.cc:179 in void lm::builder::{anonymous}::ComplainDisallowed(StringPiece, lm::WarningAction&) threw FormatLoadException.
# Special word <s> is not allowed in the corpus.  I plan to support models containing <unk> in the future.  Pass --skip_symbols to convert these symbols to whitespace.
if [ ! -f "en/sources_lm.txt" ]; then
	cat en/wiki_en_lower.txt en/mls_lm.txt ${EXCLUDED_LM_SOURCE} | sed -e 's/<s>/ /g' > en/sources_lm.txt
fi;