#!/bin/sh

set -xe

export IMPORTERS_VALIDATE_LOCALE="--validate_label_locale $HOME/validate_label.py"

export LINGUA_LIBRE_QID="22"
export LINGUA_LIBRE_ISO639="eng"
export LINGUA_LIBRE_ENGLISH="English"
export LINGUA_LIBRE_SKIPLIST="$HOME/${MODEL_LANGUAGE}/lingua_libre_skiplist.txt"

export LM_ICONV_LOCALE="en_US.UTF-8"

export MODEL_EXPORT_ZIP_LANG="en-custom"

export ENGLISH_COMPATIBLE=1