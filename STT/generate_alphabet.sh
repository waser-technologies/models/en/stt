#!/bin/bash

set -xe

joinByChar() {
  local IFS="$1"
  shift
  echo "$*"
}


pushd ${STT_DIR}
	all_train_csv="$(find /mnt/extracted/${LANGUAGE}/data/ -type f -name '*train.csv' -printf '%p,' | sed -e 's/,$//g')"
	all_dev_csv="$(find /mnt/extracted/${LANGUAGE}/data/ -type f -name '*dev.csv' -printf '%p,' | sed -e 's/,$//g')"
	all_test_csv="$(find /mnt/extracted/${LANGUAGE}/data/ -type f -name '*test.csv' -printf '%p,' | sed -e 's/,$//g')"
	csv_files=$(joinByChar , $all_train_csv $all_dev_csv $all_test_csv )
	
	mkdir -p /mnt/models/${LANGUAGE} || true

	if [ ! -f "/mnt/models/${LANGUAGE}/alphabet.txt" ]; then
		if [ "${ENGLISH_COMPATIBLE}" = "1" ]; then
			cp data/alphabet.txt /mnt/models/${LANGUAGE}/alphabet.txt
		else
			python -m coqui_stt_training.util.check_characters \
				--csv-files ${all_train_csv},${all_dev_csv},${all_test_csv} \
				--alphabet-format | grep -v '^#' | sort -n > /mnt/models/${LANGUAGE}/alphabet.txt
		fi;
	fi;
popd