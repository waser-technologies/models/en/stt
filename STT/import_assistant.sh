#!/bin/bash

set -xe

if [ ! -f "/mnt/helpers/.stt.export.lm.management" ]; then
	echo "Assistant: [Manager] has not exported data for STT"
	echo
	echo "     {!} : Training needs to envolve the language model."
	exit 1
fi;

if [ "${LM_ADD_EXCLUDED_MAX_SEC}" != "1" ]; then
	echo "Assistant: Set \$LM_ADD_EXCLUDED_MAX_SEC to 1"
	echo
	echo "     {!} : Training needs to envolve the language model."
	exit 1
fi;

if [ ! -f "/mnt/extracted/${LANGUAGE}/_assistant_lm_.txt" ]; then
	mkdir -p /mnt/helpers || true

	python ${HOMEDIR}/import_assistant.py \
		--lang "${LANGUAGE}" \
		--filter_alphabet "/mnt/models/${LANGUAGE}/alphabet.txt" \
		--normalize \
		${IMPORTERS_VALIDATE_LOCALE} \
		"/mnt/STT/helpers/.stt.export.mangement"
fi;