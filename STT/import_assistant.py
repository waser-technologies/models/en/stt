import pickle, os, sys
import unicodedata

from coqui_stt_ctcdecoder import Alphabet
from coqui_stt_training.util.importers import get_validate_label, get_importers_parser

def load_pickle(filepath, or_else=None):
	if os.path.isfile(filepath):
		with open(filepath, 'rb') as f:
			p = pickle.load(f)
			f.close()
			if p:
				return p
	return or_else

def load_lm():
	lm_path = f"/mnt/helpers/.stt.export.lm.management"
	if not os.path.isfile(lm_path):
		raise ImportError(f"Not such file: {lm_path}")
	ex = load_pickle(lm_path, or_else={})
	return ex

def label_filter(label, label_validation, alphabet=None, normalize=False):
	if normalize:
		label = (
			unicodedata.normalize("NFKD", label.strip())
			.encode("ascii", "ignore")
			.decode("ascii", "ignore")
		)
	label = label_validation(label)
	if alphabet and label and not alphabet.CanEncode(label):
		label = None
	return label

def filter_sentences(sentences, alphabet=None):
	norm_labels = []
	ALPHABET = Alphabet(alphabet) if alphabet else None
	validate_label = get_validate_label(ARGS)

	for l in sentences:
		norm_label = label_filter(l, validate_label, alphabet=ALPHABET, normalize=ARGS.normalize)
		if norm_label:
			norm_labels.append(norm_label)
	return norm_labels

def write_txt(sentences, txt_path):
	#header = "wav_filename,wav_filesize,transcript\n"
	s = '\n'.join(filter_sentences(sentences))
	with open(txt_path, 'w') as f:
		f.write(s)

def main(ARGS):
	# Import language model
	sentences=load_lm().get(ARGS.lang, []) #['one random unique sentence', 'another one', 'see it is different']
	assistant_lm_path=f"/mnt/extracted/{ARGS.lang}/_assistant_lm.txt"
	write_txt(sentences, assistant_lm_path)

if __name__ == '__main__':
	import argparse
	parser = get_importers_parser(description="Assistant's data importer for STT")

	parser.add_argument(dest="target_dir", help="Destination directory")
	parser.add_argument(
		"--filter_alphabet",
		help="Exclude samples with characters not in provided alphabet",
	)
	parser.add_argument(
		"--normalize",
		action="store_true",
		help="Converts diacritic characters to their base ones",
	)
	parser.add_argument(
		"--lang",
		type=str,
		help="Language for the LM ",
		required=True
	)

	ARGS = parser.parse_args()

	try:
		main(ARGS)
	except KeyboardInterrupt:
		print(f"{USERNAME}: \'[Ctrl] + [C]\' as Keyboard Interrupt -> Importer:STT:Assistant")
		sys.exit(1)

