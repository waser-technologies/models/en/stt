# Train custom models for english STT

This project aims to help you train your very own models tailored to your voice and application of STT.


## Build image

```zsh
$  cd STT/
$  docker build -f Dockerfile.train -t assistant-stt-en-train:latest .
```

## Publish the image

```zsh
export DOCKER_USER=wasertech

docker login -u $DOCKER_USER

docker tag assistant-stt-en-train:latest ${DOCKER_USER}/assistant-stt-en-train:latest

docker push ${DOCKER_USER}/assistant-stt-en-train:latest
```

## Start training

```zsh
$  docker run [args] ${DOCKER_USER}/assistant-stt-en-train:latest .
```

Even simpler! Checkout [`stt.train`](model.train) as your personal training wizard.

```

                 ______           _       _                _       ___                      __
                /_  __/________ _(_)___  (_)___  ____ _   | |     / (_)___  ____ __________/ /
                 / / / ___/ __ `/ / __ \/ / __ \/ __ `/   | | /| / / /_  / / __ `/ ___/ __  / 
                / / / /  / /_/ / / / / / / / / / /_/ /    | |/ |/ / / / /_/ /_/ / /  / /_/ /  
               /_/ /_/   \__,_/_/_/ /_/_/_/ /_/\__, /     |__/|__/_/ /___/\__,_/_/   \__,_/   
                                              /____/                                          


                                                [ STT ]



Assistant: Requirements are installed.

     (i) : Language is set to en.

     (i) : Checking training directory

     (i) : Tip

         : Try to contribute your voice to Common Voice EN.

     <a> : https://commonvoice.mozilla.org/en/speak

         : Your own data can be downloaded from CommonVoice directly after validation.

         : Use

     [>] : env LANG=en_US.UTF-8  CV_PERSONAL_FIRST_URL='' CV_PERSONAL_SECOND_URL='' stt.train

     (i) : You are about to hyperoptimize your own language model based on checkpoints for en.

         : For more information read the documentation for STT.

     <a> : https://stt.readthedocs.io/en/latest/LANGUAGE_MODEL.html

     (i) : Requirements

         : If you don't meet the following requirements you can try to continue anyway but the process will likely fail.

         :    -   [] System has at least one Nvidia GPU with a CUDA score of at least 6.0

         :        (i) More information about CUDA score for GPUs.

         :        <a> https://developer.nvidia.com/cuda-gpus

         :    -   [] System has a functional install of Docker with GPU capabilities.

         :        (i) More information about Docker and GPUs.

         :        <a> https://docs.docker.com/engine/install/linux-postinstall/

         :        <a> https://docs.docker.com/config/containers/resource_constraints/#gpu

     [>] : Press any key to continue or type [Ctrl] + [C] to exit:
```

Features inlcudes but are not limited to:

- Can download your personal CommonVoice archive as transfer-learning data

- Uses Assistant's Data Manager to export NLU domains intents to STT

- Automatically create, optimize and test a language model on your own voice for your own vocabulary

### Run the Wizard
```zsh
# Training Wizard path
trainer=~/.assistant/trainers/stt.train
# Download the Wizard
git archive --remote=git@gitlab.com:waser-technologies/models/en/stt.git HEAD | tar xvf - model.train
mv model.train $trainer
chmod +x $trainer
zsh $trainer
```

### See it in Action

[![](https://img.youtube.com/vi/hN1orPD2u0Y/2.jpg)](https://www.youtube.com/watch?v=hN1orPD2u0Y)