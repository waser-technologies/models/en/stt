image:
	@cd STT/ && \
	docker build -f Dockerfile.train -t assistant-stt-en-train:latest . && \
	docker tag assistant-stt-en-train:latest wasertech/assistant-stt-en-train:latest && \
	docker push wasertech/assistant-stt-en-train:latest && \
	echo "All done. Image has be built, tagged and pushed. Congratulation." || \
	echo "Code $?; Something went wrong."